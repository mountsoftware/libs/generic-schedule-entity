<?php

namespace Mountsoftware\GenericScheduleEntity\Tests;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ScheduleDayIntervalTest extends TestCase
{

    public function test_is_active_in_date()
    {
        $date = new \DateTime('2022-04-18 15:00:00');

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("16:00"));
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertTrue($interval->isActiveInDate($date), 'Full schedule is not active');

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertTrue($interval->isActiveInDate($date), 'Partial schedule is not active');

        $interval = new ScheduleDayInterval();
        $interval->setEndHour(new \DateTime("16:00"));
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertTrue($interval->isActiveInDate($date), 'Partial schedule is not active');

        $interval = new ScheduleDayInterval();
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertTrue($interval->isActiveInDate($date), 'Schedule is not active');
    }

    public function test_is_not_active_in_date()
    {
        $date = new \DateTime('2022-04-18 09:00:00');

        $interval = new ScheduleDayInterval();
        $interval->setState(Schedule::STATE_INACTIVE);
        Assert::assertFalse($interval->isActiveInDate($date), 'Schedule should not be active');

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("16:00"));
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertFalse($interval->isActiveInDate($date), 'Schedule should not be active');

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("16:00"));
        $interval->setState(Schedule::STATE_ACTIVE);
        Assert::assertFalse($interval->isActiveInDate($date), 'Schedule should not be active');
    }

}

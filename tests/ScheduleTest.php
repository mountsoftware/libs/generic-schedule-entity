<?php

namespace Mountsoftware\GenericScheduleEntity\Tests;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ScheduleTest extends TestCase
{

    public function test_is_active_in_date_period()
    {
        $date = new \DateTime('2022-04-18 15:00:00');

        $schedule = new Schedule();
        $schedule->setStartDate(new \DateTime("2022-04-01"));
        $schedule->setEndDate(new \DateTime("2022-04-30"));
        Assert::assertTrue($schedule->isActiveInDate($date), 'Schedule should be active in period');
    }

    public function test_is_active_in_date_day()
    {
        $date = new \DateTime('2022-04-18 15:00:00');

        $day = new ScheduleDay();
        $day->setDayOfWeek(ScheduleDay::MONDAY);

        $schedule = new Schedule();
        $schedule->getDays()->add($day);
        $schedule->setStartDate(new \DateTime("2022-04-01"));
        $schedule->setEndDate(new \DateTime("2022-04-30"));
        Assert::assertTrue($schedule->isActiveInDate($date), 'Schedule should be active in this day');
    }

    public function test_is_active_in_date_day_hours()
    {
        $date = new \DateTime('2022-04-18 15:00:00');

        $day = new ScheduleDay();
        $day->setDefaultState(Schedule::STATE_ACTIVE);

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("17:00"));

        $day->setDayOfWeek(ScheduleDay::MONDAY);
        $day->getIntervals()->add($interval);

        $schedule = new Schedule();
        $schedule->getDays()->add($day);
        $schedule->setStartDate(new \DateTime("2022-04-01"));
        $schedule->setEndDate(new \DateTime("2022-04-30"));
        Assert::assertTrue($schedule->isActiveInDate($date), 'Schedule should be active between this day\'s hours');
    }
}

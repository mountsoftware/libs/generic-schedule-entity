<?php

namespace Mountsoftware\GenericScheduleEntity\Tests;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ScheduleDayTest extends TestCase
{

    public function test_is_active_in_day()
    {
        $day = new ScheduleDay();
        $day->setDefaultState(Schedule::STATE_ACTIVE);

        $date = new \DateTime('monday');
        $day->setDayOfWeek(ScheduleDay::MONDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('tuesday');
        $day->setDayOfWeek(ScheduleDay::TUESDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('wednesday');
        $day->setDayOfWeek(ScheduleDay::WEDNESDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('thursday');
        $day->setDayOfWeek(ScheduleDay::THURSDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('friday');
        $day->setDayOfWeek(ScheduleDay::FRIDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('saturday');
        $day->setDayOfWeek(ScheduleDay::SATURDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

        $date = new \DateTime('sunday');
        $day->setDayOfWeek(ScheduleDay::SUNDAY);
        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');

    }

    public function test_is_active_between_day_hours()
    {
        $date = new \DateTime('2022-04-18 15:00:00');

        $day = new ScheduleDay();
        $day->setDefaultState(Schedule::STATE_ACTIVE);

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("17:00"));

        $day->setDayOfWeek(ScheduleDay::MONDAY);
        $day->getIntervals()->add($interval);

        Assert::assertTrue($day->isActiveInDate($date), 'Schedule day should be active');
    }

    public function test_is_not_active_between_day_hours()
    {
        $date = new \DateTime('2022-04-18 18:00:00');

        $day = new ScheduleDay();
        $day->setDefaultState(Schedule::STATE_ACTIVE);

        $interval = new ScheduleDayInterval();
        $interval->setStartHour(new \DateTime("10:00"));
        $interval->setEndHour(new \DateTime("17:00"));

        $day->setDayOfWeek(ScheduleDay::MONDAY);
        $day->getIntervals()->add($interval);

        Assert::assertFalse($day->isActiveInDate($date), 'Schedule day should not be active');
    }

}

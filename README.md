## Description

A generic model for schedule management. Recommended for projects using `php >= 7.4` and `symfony 4.4+`.

## How to use

- add this lib to your `composer.json`

```json
{
    "require": {
        "mountsoftware/generic-schedule-entity": "^1.0"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/mountsoftware/libs/generic-schedule-entity.git"
        }
    ]
}
```

- configure your `doctrine.yaml` to read these entities

```yaml
doctrine:

    orm:
        mappings:
            Mountsoftware\GenericScheduleEntity:
                is_bundle: false
                type: annotation
                dir: '%kernel.project_dir%/vendor/mountsoftware/generic-schedule-entity/src/Entity'
                prefix: 'Mountsoftware\GenericScheduleEntity'
                alias: MountsoftwareGenericScheduleEntity
```

- run `php bin/console doctrine:schema:update --dump-sql` or `php bin/console doctrine:migrations:diff` to preview database changes

- if happy with the result, apply changes to database

## Sonata admin classes definition

- `services.yaml`

```yaml
    app.admin.schedule.schedule:
        class: App\Admin\Schedule\ScheduleAdmin
        tags:
            - { name: sonata.admin, manager_type: orm, model_class: Mountsoftware\GenericScheduleEntity\Entity\Schedule, group: 'Schedule', label: "Schedules" }

    app.admin.schedule.schedule_day:
        class: App\Admin\Schedule\ScheduleDayAdmin
        tags:
            - { name: sonata.admin, manager_type: orm, model_class: Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay, show_in_dashboard: false }

    app.admin.schedule.schedule_day_interval:
        class: App\Admin\Schedule\ScheduleDayIntervalAdmin
        tags:
            - { name: sonata.admin, manager_type: orm, model_class: Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval, show_in_dashboard: false }
```

- `ScheduleAdmin`

```php
<?php

namespace App\Admin\Schedule;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ScheduleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->with('General', ['class' => 'col-md-6'])
            ->add('internalName', null, ['required' => false])
            ->add('startDate', DateTimePickerType::class, ['required' => false])
            ->add('endDate', DateTimePickerType::class, ['required' => false])
            ->add('defaultState', ChoiceType::class, [
                'choices' => array_combine(Schedule::ALL_STATES, Schedule::ALL_STATES)])
            ->end();

        $form
            ->with('Schedule Days', ['class' => 'col-md-6'])
            ->add('days', CollectionType::class, [], [
                'edit'   => 'inline',
                'inline' => 'table'
            ])
            ->end();

        $form->with('Schedule Exceptions')
            ->add('exceptions', CollectionType::class, [], [
                'edit'   => 'inline',
                'inline' => 'table'
            ])
            ->end();
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add(ListMapper::NAME_ACTIONS, null, [
                'actions' => [
                    'show' => [],
                    'edit' => []
                ],
            ])
            ->add('internalName')
            ->add('startDate')
            ->add('endDate')
            ->add('defaultState');;
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('internalName')
            ->add('startDate')
            ->add('endDate')
            ->add('days')
            ->add('exceptions')
            ->add('defaultState')
            ->add('createdAt');
    }
}
```

- `ScheduleDayAdmin`

```php
<?php

namespace App\Admin\Schedule;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ScheduleDayAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('dayOfWeek', ChoiceType::class, [
                'choices' => array_combine(ScheduleDay::ALL_DAYS, ScheduleDay::ALL_DAYS)
            ])
            ->add('defaultState', ChoiceType::class, [
                'choices' => array_combine(Schedule::ALL_STATES, Schedule::ALL_STATES)
            ])
            ->add('intervals', CollectionType::class, [], [
                'edit'   => 'inline',
                'inline' => 'standard'
            ]);
    }
}
```

- `ScheduleDayIntervalAdmin`

```php
<?php

namespace App\Admin\Schedule;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class ScheduleDayIntervalAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('startHour', TimeType::class)
            ->add('endHour', TimeType::class)
            ->add('state', ChoiceType::class, [
                'choices' => array_combine(Schedule::ALL_STATES, Schedule::ALL_STATES)
            ]);
    }
}
```

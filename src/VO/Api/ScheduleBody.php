<?php

namespace Mountsoftware\GenericScheduleEntity\VO\Api;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Symfony\Component\Validator\Constraints as Assert;

class ScheduleBody
{

    public ?string $id = null;

    public ?string $internalName = null;

    public ?\DateTime $startDate = null;

    public ?\DateTime $endDate = null;

    /**
     * @var $days ScheduleDayBody[]
     */
    public array $days = [];

    /**
     * @var $days ScheduleBody[]
     */
    public array $exceptions = [];

    /**
     * @Assert\Choice(choices=Schedule::ALL_STATES, message="schedule.default_state.invalid")
     */
    public ?string $defaultState = null;

}

<?php

namespace Mountsoftware\GenericScheduleEntity\VO\Api;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Symfony\Component\Validator\Constraints as Assert;

class ScheduleDayIntervalBody
{

    public ?string $id = null;

    public ?\DateTime $startHour = null;

    public ?\DateTime $endHour = null;

    /**
     * @Assert\Choice(choices=Schedule::ALL_STATES, message="schedule.default_state.invalid")
     */
    public string $state;


}

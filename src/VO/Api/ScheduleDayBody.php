<?php

namespace Mountsoftware\GenericScheduleEntity\VO\Api;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay;
use Symfony\Component\Validator\Constraints as Assert;

class ScheduleDayBody
{

    public ?string $id = null;

    /**
     * @Assert\Choice(choices=ScheduleDay::ALL_DAYS, message="schedule_day.day_of_week.invalid")
     */
    public string $dayOfWeek;

    /**
     * @var $intervals ScheduleDayIntervalBody[]
     */
    public array $intervals = [];

    /**
     * @Assert\Choice(choices=Schedule::ALL_STATES, message="schedule_day.default_state.invalid")
     */
    public string $defaultState;


}

<?php

namespace Mountsoftware\GenericScheduleEntity\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity()
 * @ORM\Table(name="mount_schedule_days")
 */
class ScheduleDay implements ScheduleAwareInterface
{
    const MONDAY = 'Monday';
    const TUESDAY = 'Tuesday';
    const WEDNESDAY = 'Wednesday';
    const THURSDAY = 'Thursday';
    const FRIDAY = 'Friday';
    const SATURDAY = 'Saturday';
    const SUNDAY = 'Sunday';

    const ALL_DAYS = [
        self::MONDAY,
        self::TUESDAY,
        self::WEDNESDAY,
        self::THURSDAY,
        self::FRIDAY,
        self::SATURDAY,
        self::SUNDAY,
    ];

    const DAYS_NAME_INDEX_MAP = [
        'Monday' => 1,
        'Tuesday' => 2,
        'Wednesday' => 3,
        'Thursday' => 4,
        'Friday' => 5,
        'Saturday' => 6,
        'Sunday' => 0,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected string $id;

    /**
     * @ORM\Column(name="day_of_week", type="string", length=9, nullable=false)
     */
    protected string $dayOfWeek;

    /**
     * @ORM\Column(name="default_state", type="string", length=10, nullable=false, options={"default":"active"})
     */
    protected string $defaultState;

    /**
     * @ORM\ManyToOne(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\Schedule", cascade={"persist"}, inversedBy="days")
     * @ORM\JoinColumn(name="schedule_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected Schedule $schedule;

    /**
     * @ORM\OneToMany(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval", cascade={"persist", "remove"},
     *     orphanRemoval=true, mappedBy="scheduleDay")
     * @var Collection|ArrayCollection|array|ScheduleDayInterval[]
     */
    protected $intervals;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default":"CURRENT_TIMESTAMP"})
     */
    protected \DateTime $createdAt;

    public function __construct()
    {
//        $this->id = Uuid::uuid4()->toString();
        $this->defaultState = Schedule::STATE_ACTIVE;
        $this->createdAt = new \DateTime();
        $this->intervals = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->dayOfWeek;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDayOfWeek(): string
    {
        return $this->dayOfWeek;
    }

    /**
     * @param string $dayOfWeek
     */
    public function setDayOfWeek(string $dayOfWeek): void
    {
        $this->dayOfWeek = $dayOfWeek;
    }

    /**
     * @return string
     */
    public function getDefaultState(): string
    {
        return $this->defaultState;
    }

    /**
     * @param string $defaultState
     */
    public function setDefaultState(string $defaultState): void
    {
        $this->defaultState = $defaultState;
    }

    /**
     * @return Schedule
     */
    public function getSchedule(): Schedule
    {
        return $this->schedule;
    }

    /**
     * @param Schedule $schedule
     */
    public function setSchedule(Schedule $schedule): void
    {
        $this->schedule = $schedule;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array|ArrayCollection|Collection|ScheduleDayInterval[]
     */
    public function getIntervals()
    {
        return $this->intervals;
    }

    /**
     * @param array|ArrayCollection|Collection|ScheduleDayInterval[] $intervals
     */
    public function setIntervals($intervals): void
    {
        $this->intervals = $intervals;
    }

    /**
     * @param ScheduleDayInterval $interval
     */
    public function addInterval(ScheduleDayInterval $interval): void
    {
        $interval->setScheduleDay($this);
        $this->getIntervals()->add($interval);
    }

    public function isActiveInDate(\DateTime $date): bool
    {
        if ($this->defaultState !== Schedule::STATE_ACTIVE) {
            return false;
        }
        if ($this->dayOfWeek !== $date->format('l')) {
            return false;
        }
        if ($this->getIntervals()->isEmpty()) {
            return true;
        }

        $activeIntervals = $this->getIntervals()->filter(function (ScheduleDayInterval $interval) use ($date) {
            return $interval->isActiveInDate($date);
        });

        return !$activeIntervals->isEmpty();
    }

    /**
     * @param $id
     * @return ScheduleDayInterval | false
     */
    public function getIntervalById($id)
    {
        return $this->getIntervals()->filter(function ($day) use ($id) {
            return $day->getId() == $id;
        })->first();
    }
}

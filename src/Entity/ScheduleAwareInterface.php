<?php

namespace Mountsoftware\GenericScheduleEntity\Entity;

interface ScheduleAwareInterface
{

    /**
     * Checks whether the class has an active period for the given date
     */
    public function isActiveInDate(\DateTime $date): bool;

}

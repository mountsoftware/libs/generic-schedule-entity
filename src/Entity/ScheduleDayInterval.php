<?php

namespace Mountsoftware\GenericScheduleEntity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity()
 * @ORM\Table(name="mount_schedule_day_intervals")
 */
class ScheduleDayInterval implements ScheduleAwareInterface
{

    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected string $id;

    /**
     * @ORM\Column(name="start_hour", type="time", nullable=true)
     */
    protected ?\DateTime $startHour = null;

    /**
     * @ORM\Column(name="end_hour", type="time", nullable=true)
     */
    protected ?\DateTime $endHour = null;

    /**
     * @ORM\Column(name="state", type="string", length=10, nullable=false, options={"default":"active"})
     */
    protected string $state;

    /**
     * @ORM\ManyToOne(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay", cascade={"persist"}, inversedBy="intervals")
     * @ORM\JoinColumn(name="schedule_day_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected ScheduleDay $scheduleDay;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default":"CURRENT_TIMESTAMP"})
     */
    protected \DateTime $createdAt;

    public function __construct()
    {
//        $this->id = Uuid::uuid4()->toString();
        $this->state = Schedule::STATE_ACTIVE;
        $this->createdAt = new \DateTime();
    }

    public function __toString()
    {
        if ($this->startHour && $this->endHour) {
            return sprintf('%s - %s',
                $this->startHour->format('H:i'),
                $this->endHour->format('H:i'));
        }

        return sprintf('%s - %s', $this->scheduleDay, $this->state);
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartHour(): ?\DateTime
    {
        return $this->startHour;
    }

    /**
     * @param \DateTime|null $startHour
     */
    public function setStartHour(?\DateTime $startHour): void
    {
        $this->startHour = $startHour;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndHour(): ?\DateTime
    {
        return $this->endHour;
    }

    /**
     * @param \DateTime|null $endHour
     */
    public function setEndHour(?\DateTime $endHour): void
    {
        $this->endHour = $endHour;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return ScheduleDay
     */
    public function getScheduleDay(): ScheduleDay
    {
        return $this->scheduleDay;
    }

    /**
     * @param ScheduleDay $scheduleDay
     */
    public function setScheduleDay(ScheduleDay $scheduleDay): void
    {
        $this->scheduleDay = $scheduleDay;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function isActiveInDate(\DateTime $date): bool
    {
        if ($this->state !== Schedule::STATE_ACTIVE) {
            return false;
        }
        if (!($this->startHour && $this->endHour)) {
            return true;
        }
        $format = 'H:i:s';

        return $this->startHour->format($format) <= $date->format($format) && $this->endHour->format($format) >= $date->format($format);
    }
}

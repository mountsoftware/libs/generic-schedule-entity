<?php

namespace Mountsoftware\GenericScheduleEntity\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity()
 * @ORM\Table(name="mount_schedules")
 * @ORM\HasLifecycleCallbacks()
 */
class Schedule implements ScheduleAwareInterface
{
    const STATE_ACTIVE = 'active';
    const STATE_INACTIVE = 'inactive';

    const ALL_STATES = [
        self::STATE_ACTIVE,
        self::STATE_INACTIVE,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected string $id;

    /**
     * @ORM\Column(name="internal_name", type="string", nullable=true)
     */
    protected ?string $internalName = null;

    /**
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    protected ?\DateTime $startDate = null;

    /**
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    protected ?\DateTime $endDate = null;

    /**
     * @ORM\OneToMany(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay", cascade={"persist", "remove"},
     *     orphanRemoval=true, mappedBy="schedule")
     * @var Collection|ArrayCollection|array|ScheduleDay[]
     */
    protected $days;

    /**
     * @ORM\ManyToOne(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\Schedule", cascade={"persist"}, inversedBy="exceptions")
     * @ORM\JoinColumn(name="parent_schedule_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected ?Schedule $parentSchedule = null;

    /**
     * @ORM\OneToMany(targetEntity="Mountsoftware\GenericScheduleEntity\Entity\Schedule", cascade={"persist", "remove"},
     *     orphanRemoval=true, mappedBy="parentSchedule")
     * @var Collection|ArrayCollection|array|Schedule[]
     */
    protected $exceptions;

    /**
     * @ORM\Column(name="default_state", type="string", length=10, nullable=false, options={"default":"active"})
     */
    protected string $defaultState;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default":"CURRENT_TIMESTAMP"})
     */
    protected \DateTime $createdAt;

    public function __construct()
    {
//        $this->id = Uuid::uuid4()->toString();
        $this->days = new ArrayCollection();
        $this->exceptions = new ArrayCollection();
        $this->defaultState = Schedule::STATE_ACTIVE;
        $this->createdAt = new \DateTime();
    }

    public function __toString()
    {
        return (string)$this->internalName;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getInternalName(): ?string
    {
        return $this->internalName;
    }

    /**
     * @param string|null $internalName
     */
    public function setInternalName(?string $internalName): void
    {
        $this->internalName = $internalName;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime|null $startDate
     */
    public function setStartDate(?\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime|null $endDate
     */
    public function setEndDate(?\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return array|ArrayCollection|Collection|ScheduleDay[]
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @return ScheduleDay | false
     */
    public function getDayById($id)
    {
        return $this->getDays()->filter(function ($day) use ($id) {
            return $day->getId() == $id;
        })->first();
    }

    /**
     * @return bool
     */
    public function hasUniqueDays(): bool
    {
        return count(array_unique($this->getDays()->map(function ($day) {
                return $day->getDayOfWeek();
            })->toArray())) == $this->getDays()->count();
    }

    /**
     * @param array|ArrayCollection|Collection|ScheduleDay[] $days
     */
    public function setDays($days): void
    {
        $this->days = $days;
    }

    /**
     * @param ScheduleDay $day
     */
    public function addDay(ScheduleDay $day): void
    {
        $day->setSchedule($this);
        $this->getDays()->add($day);
    }

    /**
     * @return Schedule|null
     */
    public function getParentSchedule(): ?Schedule
    {
        return $this->parentSchedule;
    }

    /**
     * @param Schedule|null $parentSchedule
     */
    public function setParentSchedule(?Schedule $parentSchedule): void
    {
        $this->parentSchedule = $parentSchedule;
    }

    /**
     * @return array|ArrayCollection|Collection|Schedule[]
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }

    /**
     * @param array|ArrayCollection|Collection|Schedule[] $exceptions
     */
    public function setExceptions($exceptions): void
    {
        $this->exceptions = $exceptions;
    }

    /**
     * @param Schedule $exceptions
     */
    public function addException(Schedule $exception): void
    {
        $exception->setParentSchedule($this);
        $this->getExceptions()->add($exception);
    }

    /**
     * @return string
     */
    public function getDefaultState(): string
    {
        return $this->defaultState;
    }

    /**
     * @param string $defaultState
     */
    public function setDefaultState(string $defaultState): void
    {
        $this->defaultState = $defaultState;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function isActiveInDate(\DateTime $date): bool
    {
        if ($this->defaultState !== Schedule::STATE_ACTIVE) {
            return false;
        }

        $dateFormat = 'Y-m-d';

        if (!($this->startDate && $this->endDate)) {
            $isInPeriod = true;
        } else {
            $isInPeriod = $this->startDate->format($dateFormat) <= $date->format($dateFormat)
                && $this->endDate->format($dateFormat) >= $date->format($dateFormat);
        }

        if ($this->getDays()->isEmpty()) {
            return $isInPeriod;
        }

        /** @var ScheduleDay[]|ArrayCollection $days */
        $days = $this->getDays()->filter(function (ScheduleDay $day) use ($date) {
            return $day->isActiveInDate($date);
        });

        return $isInPeriod && $days->count() === 1 && $days->first()->isActiveInDate($date);
    }

}

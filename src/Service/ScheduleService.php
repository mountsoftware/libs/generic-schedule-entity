<?php

namespace Mountsoftware\GenericScheduleEntity\Service;

use Mountsoftware\GenericScheduleEntity\Entity\Schedule;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDay;
use Mountsoftware\GenericScheduleEntity\Entity\ScheduleDayInterval;
use Mountsoftware\GenericScheduleEntity\VO\Api\ScheduleBody;

class ScheduleService
{

    /**
     * @param ScheduleBody $scheduleVO
     * @return Schedule
     */
    static function createScheduleFromVO(ScheduleBody $scheduleVO): Schedule
    {
        $schedule = new Schedule();

        $schedule->setInternalName($scheduleVO->internalName);
        $schedule->setStartDate($scheduleVO->startDate);
        $schedule->setEndDate($scheduleVO->endDate);
        $schedule->setDefaultState($scheduleVO->defaultState);

        foreach ($scheduleVO->days as $d) {
            $day = new ScheduleDay();

            $day->setDayOfWeek($d->dayOfWeek);
            $day->setDefaultState($d->defaultState);

            $schedule->addDay($day);

            foreach ($d->intervals as $i) {
                $interval = new ScheduleDayInterval();

                $interval->setEndHour($i->endHour);
                $interval->setStartHour($i->startHour);
                $interval->setState($i->state);

                $day->addInterval($interval);
            }
        }

        foreach ($scheduleVO->exceptions as $e) {
            $exception = self::createScheduleFromVO($e);
            $schedule->addException($exception);
        }

        return $schedule;

    }

}
